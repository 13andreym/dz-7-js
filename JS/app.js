const arrDZ = ['hello', 'world', 23, '23', null, {name: 'Andrey'}];

function filterBy(arr, typeOfItems) {
    
    const filteredArrDZ = arr.filter(item => typeof item !== typeOfItems);

    return filteredArrDZ;
}

const newArrDZ = filterBy(arrDZ, 'string');
console.log(newArrDZ);
